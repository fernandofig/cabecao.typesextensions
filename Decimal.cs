﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace Cabecao.TypesExtensions {
	public static class DecimalExtensions {
		private static CultureInfo EN_US_Culture = CultureInfo.GetCultureInfo("en-US");

		public static string FormatNumber(this decimal val, int maxCD) {
			return FormatNumber(val, maxCD, System.Threading.Thread.CurrentThread.CurrentCulture.Name);
		}

		public static string FormatNumber(this decimal val, int maxCD, bool groupSeparator) {
			return FormatNumber(val, maxCD, groupSeparator, System.Threading.Thread.CurrentThread.CurrentCulture.Name);
		}

		public static string FormatNumber(this decimal val, int maxCD, int minCD) {
			return FormatNumber(val, maxCD, minCD, false, System.Threading.Thread.CurrentThread.CurrentCulture.Name);
		}

		public static string FormatNumber(this decimal val, int maxCD, int minCD, bool groupSeparator) {
			return FormatNumber(val, maxCD, minCD, groupSeparator, System.Threading.Thread.CurrentThread.CurrentCulture.Name);
		}

		public static string FormatNumber(this decimal val, int maxCD, string culture) {
			return FormatNumber(val, maxCD, maxCD, false, culture);
		}

		public static string FormatNumber(this decimal val, int maxCD, bool groupSeparator, string culture) {
			return FormatNumber(val, maxCD, maxCD, groupSeparator, culture);
		}

		public static string FormatNumber(this decimal val, int maxCD, int minCD, bool groupSeparator, string culture) {
			CultureInfo cltObj = System.Globalization.CultureInfo.GetCultureInfo(culture);

			if (maxCD == minCD || maxCD < minCD)
				return string.Format(cltObj, "{0:" + (groupSeparator ? "#," : "") + "0." + new string('0', minCD) + "}", val);
			else {
				int opcCD = maxCD - minCD;
				return string.Format(cltObj, "{0:" + (groupSeparator ? "#," : "") + "0." + new string('0', minCD) + new string('#', opcCD) + "}", val);
			}
		}
		
		public static string FormatCurrency(this decimal val, string culture) {
			return val.ToString("C", System.Globalization.CultureInfo.GetCultureInfo(culture));
		}

		public static decimal ParseIntl(string value) {
			return ParseIntl(value, NumberStyles.Number);
		}

		public static decimal ParseIntl(string value, NumberStyles numberStyles) {
			return decimal.Parse(value, numberStyles, EN_US_Culture);
		}

		public static string ToIntlString(this decimal val) {
			return val.ToIntlString(null);
		}

		public static string ToIntlString(this decimal val, string format) {
			return (string.IsNullOrWhiteSpace(format) ? val.ToString(EN_US_Culture) : val.ToString(format, EN_US_Culture));
		}
	}
}