﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Security.Cryptography;

using System.Collections;

namespace Cabecao.TypesExtensions {
	public static class StringExtensions {
		public static bool IsEmail(this string Email) {
			Email = (Email + " ").Trim();
			string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
				@"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
				@".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(strRegex);
			if (re.IsMatch(Email))
				return (true);
			else
				return (false);
		}

		public static string PegaPalavras(this string nome, int numPalavras) {
			nome = (nome + " ").Trim();
			nome = System.Text.RegularExpressions.Regex.Replace(nome, @"\ {2,}", " ");

			string[] pals = nome.Split(' ');

			IList<string> palFinal = new List<string>();
			numPalavras = (numPalavras > pals.Length ? pals.Length : numPalavras);
			for (int ix = 0; ix < numPalavras; ix++) palFinal.Add(pals[ix]);

			return string.Join(" ", palFinal.ToArray());
		}

		public static string TruncaPalavras(this string txt, int maxPalavras) {
			System.Collections.ArrayList txtTrunc = new System.Collections.ArrayList();

			string[] palavras = txt.Split(' ');

			int cntWrd = 0;

			for (int i = 0; i < palavras.Length; i++) {
				if (palavras[i].Length > 2) {
					cntWrd++;
					txtTrunc.Add(palavras[i]);
					if (cntWrd >= maxPalavras) break;
				} else {
					txtTrunc.Add(palavras[i]);
				}
			}

			return String.Join(" ", (string[])txtTrunc.ToArray(typeof(string)));
		}

		public static string TruncaString(this string txt, int maxChars) {
			txt = (txt + " ").Trim();

			if (txt.Length > maxChars)
				return txt.Substring(0, maxChars - 3) + "...";
			else
				return txt;
		}

		public static bool IsAnyOf(this string txt, params string[] stringExprs) {
			return stringExprs.Contains(txt);
		}

		/// <summary>
		/// Retorna uma string com valor normalizado: Com espaços à esquerda e direita cortados, e se o resultado for um valor vazio, normaliza o valor para null
		/// </summary>
		public static string NormalizaEspaco(this string str, bool fazTrim = true, bool checkNull = true) {
			if(fazTrim) str = (str + " ").Trim();
			return (string.IsNullOrEmpty(str) && checkNull ? null : str);
		}

		public static string RemoveAcentos(this string source) {
			string sourceInFormD = source.Normalize(NormalizationForm.FormD);

			var output = new StringBuilder();
			foreach (char c in sourceInFormD) {
				UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(c);
				if (uc != UnicodeCategory.NonSpacingMark)
					output.Append(c);
			}

			return (output.ToString().Normalize(NormalizationForm.FormC));
		}

		public static string NormalizaNomeArquivo(this string fn) {
			string semSimb = Regex.Replace(fn.RemoveAcentos().Trim(), "[!@#$%¨&*()=_+\\[\\]{},.;/<>:?^~´`'\"\\\\|]", "");
			string semEspaco = Regex.Replace(semSimb, "\\s{2,}", " ");

			return semEspaco.Replace(" ", "_");
		}

		public static string GetHash_SHA1(this string msg) {
			SHA1Managed sha1Hasher = new SHA1Managed();

			byte[] ByteHash = sha1Hasher.ComputeHash(System.Text.Encoding.UTF8.GetBytes(msg));

			string hash = System.BitConverter.ToString(ByteHash).Replace("-", string.Empty);
			return hash;
		}

		public static string ToTitleCaseBR(this string str, bool processSentences = true) {
			if (string.IsNullOrEmpty(str) || str.IndexOf(' ') == -1) return str;
			string baseStr = str.ToLower();
			string[] preposicoesEArtigos = new string[] { "a", "o", "e", "da", "de", "do" };
			string[] sentencas = baseStr.Split('.');

			for (int s = 0; s < sentencas.Length; s++) {
				string[] palavras = sentencas[s].Split(' ');
				bool primPalavra = processSentences;

				for (int w = 0; w < palavras.Length; w++) {
					if (primPalavra || !preposicoesEArtigos.Contains(palavras[w])) {
						char[] chArray = palavras[w].ToCharArray();

						if (chArray.Length >= 1) {
							int pl;

							for (pl = 0; pl < chArray.Length; pl++)
								if (char.IsLetter(chArray[pl])) break;

							if (char.IsLower(chArray[pl]))
								chArray[pl] = char.ToUpper(chArray[pl]);

							primPalavra = false;
						}

						palavras[w] = new string(chArray);
					}
				}

				sentencas[s] = string.Join(" ", palavras);
			}

			return string.Join(".", sentencas);
		}
	}
}