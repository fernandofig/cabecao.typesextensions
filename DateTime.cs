﻿using System;

namespace Cabecao.TypesExtensions {
	public static class DateTimeExtensions {
		public static DateTime? GetFromString(this DateTime? dt, string dateTime) {
			dateTime = (dateTime + " ").Trim();
			return (string.IsNullOrEmpty(dateTime) ? null : (DateTime?)DateTime.Parse(dateTime));
		}

		public static int LastDayFromMonth(this DateTime dt) {
			return DateTime.DaysInMonth(dt.Year, dt.Month);
		}

		public static DateTime EndOfDay(this DateTime dt) {
			return dt
					.AddMilliseconds(-dt.Millisecond) // Go back the clock to the start of the day...
					.AddSeconds(-dt.Second)
					.AddMinutes(-dt.Minute)
					.AddHours(-dt.Hour) 
					.AddDays(1) // ... Then add a day...
					.AddMilliseconds(-1); // ... and substract a millisecond. Presto!
		}

		public static DateTime AddDiasUteis(this DateTime dt, double dias) {
			DateTime novaDt = dt.AddDays(dias);

			while (novaDt.DayOfWeek == DayOfWeek.Saturday || novaDt.DayOfWeek == DayOfWeek.Sunday || novaDt.IsFeriadoNacional()) novaDt = novaDt.AddDays(1);

			return novaDt;
		}

		public static bool TryParseExactCurrentCulture(string s, out DateTime result) {
			return DateTime.TryParseExact(
									s,
									System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern,
									System.Threading.Thread.CurrentThread.CurrentCulture,
									System.Globalization.DateTimeStyles.None,
									out result);
		}

		public static string ToSortableString(this DateTime dt) {
			return dt.ToString("s", System.Globalization.CultureInfo.InstalledUICulture);
		}

		public static DateTime FromSortableString(string s) {
			return DateTime.ParseExact(s, "s", System.Globalization.CultureInfo.InstalledUICulture);
		}

		public static bool IsFeriadoNacional(this DateTime dt) {
			object[] feriados = new object[] {
				new int[] { 1, 1 },   // Confraternização Universal
				new int[] { 21, 4 },  // Tiradentes
				new int[] { 1, 5 },   // Dia do Trabalho
				new int[] { 7, 9 },   // Independência
				new int[] { 12, 10 }, // Ns. Sra. Aparecida
				new int[] { 02, 11 }, // Finados
				new int[] { 15, 11 }, // Procl. República
				new int[] { 25, 12 }  // Natal
			};

			int dia = dt.Day, mes = dt.Month;

			foreach (int[] data in feriados) if (data[0] == dia && data[1] == mes) return true;

			return false;
		}

		public static DateTime FromUnixTime(this long unixTime) {
			var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
			return epoch.AddMilliseconds(unixTime);
		}

		public static long ToUnixTime(this DateTime date) {
			var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
			return Convert.ToInt64((date - epoch).TotalMilliseconds);
		}
	}
}