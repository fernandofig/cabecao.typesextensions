﻿using System;
using System.Globalization;
using System.Configuration;

namespace Cabecao.TypesExtensions {
	public class AppSettings {
		public static string GetValue(string key, string defaultValue = null) {
			string val = ConfigurationManager.AppSettings[key];

			return val ?? defaultValue;
		}

		public static T GetTypedValue<T>(string key, T defaultValue = default(T)) {
			CultureInfo cUS = CultureInfo.GetCultureInfo("en-US");
			string cfgVal = ConfigurationManager.AppSettings[key];

			if (string.IsNullOrWhiteSpace(cfgVal)) return defaultValue;

			Type ft = typeof(T);
			object val;

			if (ft == typeof(byte))
				val = byte.Parse(cfgVal);
			else if (ft == typeof(int))
				val = Int32.Parse(cfgVal);
			else if (ft == typeof(decimal))
				val = Decimal.Parse(cfgVal, cUS);
			else if (ft == typeof(double))
				val = Double.Parse(cfgVal, cUS);
			else if (ft == typeof(DateTime))
				val = DateTime.Parse(cfgVal);
			else if (ft == typeof(bool))
				val = bool.Parse(cfgVal);
			else
				val = Convert.ChangeType(cfgVal, typeof(T));

			return (T)val;
		}
	}
}
