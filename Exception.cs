﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

using System.Collections;

namespace Cabecao.TypesExtensions {
	public static class ExceptionExtensions {
		public static string GetDetails(this System.Exception ex) {
			System.Exception fex, tex = ex;
			do {
				fex = tex;
				tex = tex.InnerException;
			} while (tex != null && !string.IsNullOrEmpty(tex.Message));

			return fex.Message;
		}
	}
}