﻿using System.Threading;
using System.Globalization;

namespace System.Threading {
	public static class ThreadCultureExtensions {
		public static void SetCurrentCulture(this Thread t, CultureInfo lang) {
			t.CurrentCulture = lang;
			t.CurrentUICulture = lang;
		}
	}
}